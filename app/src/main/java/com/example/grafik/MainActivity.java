package com.example.grafik;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    Realm realm;
    EditText kucukTansiyon, buyukTansiyon;
    Button ekle;
    BarChart barChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        realm = Realm.getDefaultInstance();
        tanimla();
        ekleData();
        listele();
        show();

    }


    public void tanimla() {
        buyukTansiyon = findViewById(R.id.buyukTansiyon);
        kucukTansiyon = findViewById(R.id.kucuktansiyon);
        ekle = findViewById(R.id.ekleButton);
        barChart = findViewById(R.id.barChart);
    }


    public void ekleData() {
        ekle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        Tansiyon tansiyon = realm.createObject(Tansiyon.class);
                        tansiyon.setBuyukTansiyon(buyukTansiyon.getText().toString());
                        tansiyon.setKucukTansiyon(kucukTansiyon.getText().toString());
                    }
                });
                listele();
            }
        });
    }


    public void listele() {

        RealmResults<Tansiyon> list = realm.where(Tansiyon.class).findAll();

        for (Tansiyon t : list) {


            Log.i("Data : ", t.toString());
        }
    }

    public void show() {
        RealmResults<Tansiyon> list = realm.where(Tansiyon.class).findAll();

        Float buyukTansiyon = Float.parseFloat(list.get(0).getBuyukTansiyon());
        Float kucukTansiyon = Float.parseFloat(list.get(0).getKucukTansiyon());
        ArrayList<BarEntry> arrayList = new ArrayList<>();
        arrayList.add(new BarEntry(buyukTansiyon, 0));
        arrayList.add(new BarEntry(kucukTansiyon, 1));
        BarDataSet barDataSet = new BarDataSet(arrayList, "Toplam Deger");

        ArrayList<String> sutunIsmi = new ArrayList<>();
        sutunIsmi.add("Buyuk Tansiyon");
        sutunIsmi.add("Kucuk Tansiyon");

        BarData barData = new BarData(sutunIsmi, barDataSet);
        barChart.setData(barData);
        barChart.setDescription("Tansiyon degerlerini olcen grafik arayuzudur.");
    }
}
