package com.example.grafik;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

@RealmClass
public class Tansiyon extends RealmObject {

    private String kucukTansiyon, buyukTansiyon;


    @Override
    public String toString() {
        return "Tansiyon{" +
                "kucukTansiyon='" + kucukTansiyon + '\'' +
                ", buyukTansiyon='" + buyukTansiyon + '\'' +
                '}';
    }

    public String getKucukTansiyon() {
        return kucukTansiyon;
    }

    public void setKucukTansiyon(String kucukTansiyon) {
        this.kucukTansiyon = kucukTansiyon;
    }

    public String getBuyukTansiyon() {
        return buyukTansiyon;
    }

    public void setBuyukTansiyon(String buyukTansiyon) {
        this.buyukTansiyon = buyukTansiyon;
    }
}
